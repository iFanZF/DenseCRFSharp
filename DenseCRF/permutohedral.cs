﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace DenseCRF
{
	struct Neighbors
	{
    	public	int n1, n2;
	 
	};
	public class permutohedral
	{
		float round2(float v)
		{
			return (float)Math.Floor(v + 0.5f);
		}
		int N_ = 0, d_ = 0; int M_ = 0;
		float[,,] barycentric_;
		int[,,] offset_;
		Neighbors[] blur_neighbors_;
		public unsafe void init(float[,,] feature, int feature_size, int W, int H)
		{
			N_ = W * H;
			d_ = feature_size;
			HashTable hash_table = new HashTable(d_, N_ * (d_ + 1));

			offset_ = new int[W, H, (d_ + 1)];

			barycentric_ = new float[W, H, (d_ + 1)];
			float[] scale_factor = new float[d_];
			float[] elevated = new float[d_ + 1];
			float[] rem0 = new float[d_ + 1];
			float[] barycentric = new float[d_ + 2];
			short[] rank = new short[d_ + 1];
			short[] canonical = new short[(d_ + 1) * (d_ + 1)];
			short[] key = new short[d_ + 1];
			for (int i = 0; i <= d_; i++)
			{
				for (int j = 0; j <= d_ - i; j++)
					canonical[i * (d_ + 1) + j] = (short)i;
				for (int j = d_ - i + 1; j <= d_; j++)
					canonical[i * (d_ + 1) + j] = (short)(i - (d_ + 1));
			}
			float inv_std_dev = (float)Math.Sqrt(2.0f / 3.0f) * (d_ + 1);
			// Compute the diagonal part of E (p.5 in [Adams etal 2010])
			for (int i = 0; i < d_; i++)
				scale_factor[i] = 1.0f / (float)Math.Sqrt((i + 2.0f) * (i + 1.0f)) * inv_std_dev;


			for (int s = 0; s < H; s++)
			{
				for (int k = 0; k < W; k++)
				{

					//float* f = feature + k * feature_size;
					float sm = 0;
					for (int j = d_; j > 0; j--)
					{
						float cf = feature[k, s, j - 1] * scale_factor[j - 1];
						elevated[j] = sm - j * cf;
						sm += cf;
					}
					elevated[0] = sm;

					// Find the closest 0-colored simplex through rounding
					float down_factor = 1.0f / (d_ + 1);
					float up_factor = (d_ + 1);
					int sum = 0;
					for (int i = 0; i <= d_; i++)
					{
						int rd = (int)round2(down_factor * elevated[i]);
						rem0[i] = rd * up_factor;
						sum += rd;
					}

					// Find the simplex we are in and store it in rank (where rank describes what position coorinate i has in the sorted order of the features values)
					for (int i = 0; i <= d_; i++)
						rank[i] = 0;
					for (int i = 0; i < d_; i++)
					{
						double di = elevated[i] - rem0[i];
						for (int j = i + 1; j <= d_; j++)
							if (di < elevated[j] - rem0[j])
								rank[i]++;
							else
								rank[j]++;
					}


					// If the point doesn't lie on the plane (sum != 0) bring it back
					for (int i = 0; i <= d_; i++)
					{
						rank[i] += (short)sum;
						if (rank[i] < 0)
						{
							rank[i] += (short)(d_ + 1);
							rem0[i] += d_ + 1;
						}
						else if (rank[i] > d_)
						{
							rank[i] -= (short)(d_ + 1);
							rem0[i] -= d_ + 1;
						}
					}
					for (int i = 0; i <= d_ + 1; i++)
						barycentric[i] = 0;
					for (int i = 0; i <= d_; i++)
					{
						float v = (elevated[i] - rem0[i]) * down_factor;
						barycentric[d_ - rank[i]] += v;
						barycentric[d_ - rank[i] + 1] -= v;
					}
					// Wrap around
					barycentric[0] += 1.0f + barycentric[d_ + 1];

					// Compute all vertices and their offset
					for (int remainder = 0; remainder <= d_; remainder++)
					{
						for (int i = 0; i < d_; i++)
							key[i] = (short)(rem0[i] + canonical[remainder * (d_ + 1) + rank[i]]);
						offset_[k, s, remainder] = hash_table.find(key, true);
						barycentric_[k, s, remainder] = barycentric[remainder];
					}

				}
			}

			M_ = hash_table.size();

			// Create the neighborhood structure

			blur_neighbors_ = new Neighbors[(d_ + 1) * M_];

			short[] n1 = new short[d_ + 1];
			short[] n2 = new short[d_ + 1];

			// For each of d+1 axes,
			for (int j = 0; j <= d_; j++)
			{
				for (int i = 0; i < M_; i++)
				{
					key = hash_table.getKeys(i);
					for (int k = 0; k < d_; k++)
					{
						n1[k] = (short)(key[k] - 1);
						n2[k] = (short)(key[k] + 1);
					}
					n1[j] = (short)(key[j] + d_);
					n2[j] = (short)(key[j] - d_);

					blur_neighbors_[j * M_ + i].n1 = hash_table.find(n1);
					blur_neighbors_[j * M_ + i].n2 = hash_table.find(n2);
				}
			}


		}
		public unsafe void compute(float[,,] out1, float[,,] in1, int value_size, int in_offset, int out_offset, int W, int H)

		{

			// M_ = hash_table.size();
			// Shift all values by 1 such that -1 -> 0 (used for blurring)
			//float* values=(float*)Marshal.AllocHGlobal((M_ + 2) * value_size);
			float[] values = new float[(M_ + 2) * value_size];
			float[] new_values = new float[(M_ + 2) * value_size];

			for (int i = 0; i < (M_ + 2) * value_size; i++)
				values[i] = new_values[i] = 0;

			// Splatting
			for (int s = 0; s < H; s++)
			{
				for (int k = 0; k < W; k++)
				{
					for (int j = 0; j <= d_; j++)
					{
						int o = offset_[k, s, j] + 1;
						float w = barycentric_[k, s, j];
						for (int ks = 0; ks < value_size; ks++)
							values[o * value_size + ks] += w * in1[k, s, ks];
					}
				}
			}

			for (int j = 0; j <= d_; j++)
			{
				for (int i = 0; i < M_; i++)
				{
					int old_val = (i + 1) * value_size;
					int new_val = (i + 1) * value_size;

					int n1 = blur_neighbors_[j * M_ + i].n1 + 1;
					int n2 = blur_neighbors_[j * M_ + i].n2 + 1;
					int n1_val = n1 * value_size;
					int n2_val = n2 * value_size; //float* n2_val = values + n2 * value_size; 
					for (int k = 0; k < value_size; k++)
						new_values[new_val + k] = values[old_val + k] + 0.5f * (values[n1_val + k] + values[n2_val + k]);
				}
				float[] tmp = new float[values.Length];
				Array.Copy(values, tmp, values.Length);
				values = new_values;
				new_values = tmp;
			}
			// Alpha is a magic scaling constant (write Andrew if you really wanna understand this)
			float alpha = 1.0f / (1.0f + (float)Math.Pow(2.0f, -(float)d_));

			// Slicing

			for (int s = 0; s < H; s++)
			{
				for (int k = 0; k < W; k++)
				{
					for (int ks = 0; ks < value_size; ks++)
						out1[k, s, ks] = 0;
					for (int j = 0; j <= d_; j++)
					{
						int o = offset_[k, s, j] + 1;
						float w = barycentric_[k, s, j];
						for (int ks = 0; ks < value_size; ks++)
							out1[k, s, ks] += w * values[o * value_size + ks] * alpha;
					}
				}
			}
		}
		

	}


	unsafe class HashTable
	{
		// Don't copy!
		public HashTable(int key_size, long capacity)
		{
			
			key_size_ = (uint)key_size;
			capacity_ = 2 * (ulong)capacity;
			table_ = new int[capacity_];
			keys_ = new short[(capacity_ / 2 + 10) * key_size_];
			for (int i = 0; i < table_.Length; i++)
			{
				table_[i] = -1;
			}
			//memset(table_, -1, capacity_ * sizeof(int));
		}

		UInt64 key_size_, filled;UInt64 capacity_;
		short[] keys_;
		int[] table_;
		void grow()
		{
			// Swap out the old memory
			short[] old_keys = keys_;
			int[] old_table = table_;
			UInt64 old_capacity = capacity_;
			capacity_ *= 2;
			// Allocate the new memory
			short[] newkeys_ = new short[(old_capacity + 10) * key_size_];
			int[] newtable_ = new int[capacity_];
			for (int i = 0; i < newtable_.Length; i++)
			{
				newtable_[i] = -1;
			}
			for (int i = 0; i < newkeys_.Length; i++)
			{
				newkeys_[i] = old_keys[i];
			}
			//memset(table_, -1, capacity_ * sizeof(int));
			//memcpy(keys_, old_keys, filled_ * key_size_ * sizeof(short));

			// Reinsert each element
			for (uint i = 0; i < old_capacity; i++)
				if (old_table[i] >= 0)
				{
					int e = old_table[i];
				//	old_keys + (getKey(e))
					List<short> ok = new List<short>();
					ok.AddRange(old_keys);
					ok.AddRange(getKeys(e));
					UInt64 h = hash(ok.ToArray()) % capacity_;
					for (; table_[h] >= 0; h = h < capacity_ - 1 ? h + 1 : 0) ;
					//table_[h] = e;
					newtable_[h] = e;
				}
			keys_ = newkeys_;
			table_ = newtable_;
			//delete[] old_keys;
			//delete[] old_table;
		}
		//	long hash(  short[] k ) {
		//	long r = 0;
		//	for(long i=0; i<key_size_; i++ ){
		//		r += k[i];
		//		r *= 1664525;
		//	}
		//	return r;
		//}

		public int size()
		{
			return (int)filled;
		}
		void reset()
		{
			filled = 0;
			for (int i = 0; i < table_.Length; i++)
			{
				table_[i] = -1;
			}
		}
		UInt64 hash(  short[] k ) {
		UInt64 r = 0;
		for(uint i=0; i<key_size_; i++ ){
			r += (UInt64)k[i];
			r *= 1664525;
		}
		return r;
	}
	public  ulong getKey( uint i ) {
		return i * key_size_;
	}

		public short[] getKeys(int i)
		{
			short[] temp = new short[key_size_+1];
			for (uint s =0; s < key_size_; s++)
				temp[s] = keys_[(i * (int)key_size_) + s];
			return temp;
		}

		public int find(short[] k, bool create = false)
		{
			if (2 * filled >= capacity_) grow();
			// Get the hash value
			UInt64 h = hash(k) % capacity_;
			// Find the element with he right key, using linear probing
			while (true)
			{
				int e = table_[h];
				if (e == -1)
				{
					if (create)
					{
						// Insert a new key and return the new id
						for (ulong i = 0; i < key_size_; i++)
							keys_[filled * key_size_ + i] = k[i];
						return table_[h] = (int)filled++;
					}
					else
						return -1;
				}
				// Check if the current key is The One
				bool good = true;
				for (int i = 0; i < (int)key_size_ && good; i++)
					if (keys_[e * (int)key_size_ + i] != k[i])
						good = false;
				if (good)
					return e;
				// Continue searching
				h++;
				if (h == capacity_) h = 0;
			}
		}

	}

}
